/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_expendedora.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0, cont_ms2 = 0, cont_ms3 = 0;

    hw_Init();

    fsm_expendedora_init();

    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == TECLA_1) {
            fsm_expendedora_evIngresaFicha();
        }  

        if (input == TECLA_2) {
            fsm_expendedora_evSeleccionTe();
        }

        if (input == TECLA_3) {
            fsm_expendedora_evSeleccionCafe();
        }
 
        cont_ms++;
        cont_ms2++;
        cont_ms3++;
        if(cont_ms2 == 100){
            cont_ms2 = 0;
            fsm_expendedora_evTick100mseg();
        }
        if(cont_ms3 == 500){
            cont_ms3 = 0;
            fsm_expendedora_evTick500mseg();
        }
        if (cont_ms == 1000) {
            cont_ms = 0;
            fsm_expendedora_evTick1seg();
            fsm_expendedora_printCurrentState();
        }

        fsm_expendedora_runCycle();


        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
