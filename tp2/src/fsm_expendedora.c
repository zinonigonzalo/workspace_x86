/*==================[inclusions]=============================================*/

#include "fsm_expendedora.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_EXPENDEDORA_STATES_T state;

bool evIngresaFicha;
bool evSeleccionTe;
bool evSeleccionCafe;
bool evTick100mseg;
bool evTick500mseg;
bool evTick1seg;
uint8_t count_seg;
int count_100mseg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evIngresaFicha = 0;
    evSeleccionTe = 0;
    evSeleccionCafe = 0;
    evTick100mseg = 0;
    evTick500mseg = 0;
    evTick1seg = 0;
}

/*==================[external functions definition]==========================*/

void fsm_expendedora_init(void)
{
    state = REPOSO;
    clearEvents();
}

void fsm_expendedora_runCycle(void)
{

    switch (state) {
        case REPOSO:
            if (evIngresaFicha){
                count_seg = 0;
                state = ESPERANDO_SELECCION;
            }
            if (evTick500mseg)
                //toggleLED();
            
            break;
        
        case ESPERANDO_SELECCION:
            if (evTick1seg && (count_seg < 30))
                count_seg++;

            if (evTick1seg && (count_seg == 30)){
                hw_devolverFicha();
                state = REPOSO;
            }

            if (evSeleccionTe){
                count_100mseg = 0;
                hw_servirTe();
                state = SIRVIENDO_TE;
            }

            if (evSeleccionCafe){
                count_100mseg = 0;
                hw_servirCafe();
                state = SIRVIENDO_CAFE;
            }
            break;
        
        case SIRVIENDO_TE:
            if(evTick100mseg && (count_100mseg < 300)){
                count_100mseg++;
                //toggleLED();
            }    

            if(evTick100mseg && (count_100mseg == 300)){
                count_seg = 0;
                state = INDICANDO_BEBIDA_SERVIDA;
            }
            
            break;

        case SIRVIENDO_CAFE:

            if(evTick100mseg && (count_100mseg < 450)){
                count_100mseg++;                
                //toggleLED();
            }

            if(evTick100mseg && (count_100mseg == 450)){
                count_seg = 0;
                state = INDICANDO_BEBIDA_SERVIDA;
            }

            break;

        case INDICANDO_BEBIDA_SERVIDA:
            if(evTick1seg && (count_seg < 2)){
                count_seg++;
                hw_indicadorSonido();
            }

            if(evTick1seg && (count_seg == 2))
                state = REPOSO;
            break;
    }

    clearEvents();
}

void toggleLED(void){
    printf("."); 
}

void fsm_expendedora_evIngresaFicha(void){
    evIngresaFicha = 1;
}

void fsm_expendedora_evSeleccionTe(void){
    evSeleccionTe = 1;
}

void fsm_expendedora_evSeleccionCafe(void){
    evSeleccionCafe = 1;
}

void fsm_expendedora_evTick100mseg(void){
    evTick100mseg = 1;
}

void fsm_expendedora_evTick500mseg(void){
    evTick500mseg = 1;
}

void fsm_expendedora_evTick1seg(void){
    evTick1seg = 1;
}

void fsm_expendedora_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/
