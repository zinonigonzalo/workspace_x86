#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc
#define TECLA_1  49  // ASCII para la tecla 1
#define TECLA_2  50  // ASCII para la tecla 2
#define TECLA_3  51  // ASCII para la tecla 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_devolverFicha(void);
void hw_servirTe(void);
void hw_servirCafe(void);
void hw_indicadorSonido(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
