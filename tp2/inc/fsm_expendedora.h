#ifndef _EXPENDEDORA_H_
#define _EXPENDEDORA_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    REPOSO,
    ESPERANDO_SELECCION,
    SIRVIENDO_TE,
    SIRVIENDO_CAFE,
    INDICANDO_BEBIDA_SERVIDA
} FSM_EXPENDEDORA_STATES_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void fsm_expendedora_init(void);
void fsm_expendedora_runCycle(void);

// Funcion que apaga o prende un LED segun el estado en el que se encuentra
void toggleLED(void);

// Eventos
void fsm_expendedora_evIngresaFicha(void);
void fsm_expendedora_evSeleccionTe(void);
void fsm_expendedora_evSeleccionCafe(void);
void fsm_expendedora_evTick100mseg(void);
void fsm_expendedora_evTick500mseg(void);
void fsm_expendedora_evTick1seg(void);

// Debugging
void fsm_expendedora_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _EXPENDEDORA_H_ */
