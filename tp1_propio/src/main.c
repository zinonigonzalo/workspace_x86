/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

#include <stdio.h>
#include <stdint.h>

#define REPOSO 0
#define INGRESANDO 1
#define ESPERANDO 2
#define ALARMA 3

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;

    hw_Init();

    int state=0, cont=0;

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        printf("Estado actual: %d\n",state);
        input = hw_LeerEntrada();

        switch (state){

            case REPOSO:
                if (input == SENSOR_2){
                    hw_ActivarAlarma();
                    state = ALARMA;
                }
                else if (input == SENSOR_1){
                    hw_AbrirBarrera();
                    state = INGRESANDO;
                }
            break;
        
            case INGRESANDO:
                if (input == SENSOR_2){
                    cont=0;
                    state = ESPERANDO;
                }
            break;

            case ESPERANDO:
                if (input == SENSOR_1){
                    state = INGRESANDO;
                }
                else{
                    printf("%d\n",cont);
                    cont++;
                    if(cont == 15){
                    cont=0;    
                    state = REPOSO;
                    }
                }
            break; 

            case ALARMA:
                printf("\nLA ALARMA SE DESACTIVARA EN X SEGUNDOS\n");
                hw_Pausems(1000);
                hw_DesactivarAlarma();
                state = REPOSO;
            break;
        }


        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
