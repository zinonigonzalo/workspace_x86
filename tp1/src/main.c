/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_garaje.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;

    hw_Init();

    fsm_garaje_init();

    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
            fsm_garaje_raise_evSensor1_On();
        }

        if (input == SENSOR_2) {
            fsm_garaje_raise_evSensor2_On();
        }
        else {
            fsm_garaje_raise_evSensor2_Off();
        }


        cont_ms++;
        if (cont_ms == 1000) {
            cont_ms = 0;
            fsm_garaje_raise_evTick1seg();
        }

        fsm_garaje_runCycle();


        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/

